from tkinter import *
import random
import time
class Ball:
    def __init__(self, canvas, paddle, color):
        self.canvas = canvas
        self.paddle = paddle
        self.id = canvas.create_oval(10, 10, 25, 25, fill=color)
        self.canvas.move(self.id, 245, 100)
        starts = [-3, -2, -1, 1, 2, 3]
        random.shuffle(starts)
        self.x = starts[0]
        self.y = -3
        self.canvas_height = self.canvas.winfo_height()
        self.canvas_width = self.canvas.winfo_width()
        self.hit_bottom = False
    def hit_paddle(self, pos):
        paddle_pos = self.canvas.coords(self.paddle.id)
        if pos[2] >= paddle_pos[0] and pos[0] <= paddle_pos[2]:
            if pos[3] >= paddle_pos[1] and pos[3] <= paddle_pos[3]:
                return True
        return False
    # Проверка столкновение с кирпичом
    def hit_brick(self, pos, brick):
        brick_pos = self.canvas.coords(brick.id)
        return pos[2] >= brick_pos[0] and pos[0] <= brick_pos[2] and pos[1] <= brick_pos[3]
    def draw(self, bricks):
        self.canvas.move(self.id, self.x, self.y)
        pos = self.canvas.coords(self.id)
        if pos[1] <= 0:
            self.y = 1
        if pos[3] >= self.canvas_height:
            self.hit_bottom = True
        if self.hit_paddle(pos) == True:
            self.y = -1
        # Проверка столкновения с кирпичами (цикл)
        for brick in bricks:
            if self.hit_brick(pos,brick):
                self.y = 1
        if pos[0] <= 0:
            self.x = 1
        if pos[2] >= self.canvas_width:
            self.x = -1
class Paddle:
    def __init__(self, canvas, color):
        self.canvas = canvas
        self.id = canvas.create_rectangle(0, 0, 100, 10, fill=color)
        self.canvas.move(self.id, 200, 500)
        # self.canvas.move(self.id, canvas.width/2-50, canvas.height-100)
        self.x = 0
        self.canvas_width = self.canvas.winfo_width()
        self.canvas.bind_all('<KeyPress-Left>', self.turn_left)
        self.canvas.bind_all('<KeyPress-Right>', self.turn_right)
    def draw(self):
        # self.canvas.move(self.id, self.x, 0)
        pos = self.canvas.coords(self.id)
        if pos[0] <= 0:
            self.x = 0
        elif pos[2] >= self.canvas_width:
            self.x = 0
    def turn_left(self, evt):
        self.x = -7
        self.canvas.move(self.id, self.x, 0);
    def turn_right(self, evt):
        self.x = 7
        self.canvas.move(self.id, self.x, 0);
class Brick:
    def __init__(self, canvas, color, h_coord):
        self.canvas = canvas
        self.id = canvas.create_rectangle(h_coord, 0, h_coord+50, 25, fill=color)
    # перекраска кирпича в красный
    def change_color(self):
        pass
tk = Tk()
tk.title("Игра")
# tk.resizable(0, 0)
tk.wm_attributes("-topmost", 1)
canvas = Canvas(tk, width=500, height=600, bd=0, 
highlightthickness=0)
canvas.pack()
tk.update()
paddle = Paddle(canvas, 'black')
ball = Ball(canvas, paddle, 'orange')
bricks = []
for x in range(10):
    bricks.append(Brick(canvas, 'blue', x*50))
while 1:
    if ball.hit_bottom == False:
        ball.draw(bricks)
        paddle.draw()
    # Проверка, что все кирпичи красные
    tk.update_idletasks()
    tk.update()
    time.sleep(0.01)
